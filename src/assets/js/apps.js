/*
addcustomerstype

*/
function addCustomersPersonal(){
  return '<div class="col-sm-4"><label for="" class="control-label">İsim</label><input type="text" name="Name" [(ngModel)]="model.Name" #Name="ngModel" class="form-control"></div><div class="col-sm-4"><label for="" class="control-label">Soyisim</label><input type="text" class="form-control"></div><div class="col-sm-4"><label for="" class="control-label">Tckn</label><input type="text" class="form-control"></div><div class="col-sm-4"><label for="" class="control-label">Cinsiyet</label><input type="text" class="form-control"></div><div class="col-sm-4"><label for="" class="control-label">Doğum Yeri</label><input type="text" class="form-control"></div><div class="col-sm-4"><label for="" class="control-label">Doğum Tarihi</label><input type="text" class="form-control"></div><div class="col-sm-4"><label for="" class="control-label">Eğitim Durumu</label><input type="text" class="form-control"></div><div class="col-sm-4"><label for="" class="control-label">Kimlik Seri No</label><input type="text" class="form-control"></div><div class="col-sm-4"><label for="" class="control-label">Kimlik No</label><input type="text" class="form-control"></div><div class="col-sm-4"><label for="" class="control-label">Cilt No</label><input type="text" class="form-control"></div><div class="col-sm-4"><label for="" class="control-label">Aile Sıra No</label><input type="text" class="form-control"></div><div class="col-sm-4"><label for="" class="control-label">Sıra No</label><input type="text" class="form-control"></div>';
}
function addCustomersCorparate(){
  return '<div class="col-sm-4"><label class="control-label">Şirket Adı</label><input type="text" class="form-control"></div><div class="col-sm-4"><label class="control-label">Şirket Ünvanı</label><input type="text" class="form-control"></div><div class="col-sm-4"><label class="control-label">Vergi Dairesi</label><input type="text" class="form-control"></div><div class="col-sm-4"><label class="control-label">Vergi Numarası</label><input type="text" class="form-control"></div><div class="col-sm-4"><label class="control-label">VAT Numarası</label><input type="text" class="form-control"></div><div class="col-sm-4"><label class="control-label">Kuruluş Tarihi</label><input type="text" class="form-control"></div><div>';
}
function addCustomersForeigner(){
  return '<div class="col-sm-4"><label class="control-label">Pasaport Numarası</label><input type="text" class="form-control"></div><div class="col-sm-4"><label class="control-label">İsim</label><input type="text" class="form-control"></div><div class="col-sm-4"><label class="control-label">Soyisim</label><input type="text" class="form-control"></div><div class="col-sm-4"><label class="control-label">Eğitim Durumu</label><select class="form-control"><option value="1">İlkokul</option><option value="2">Ortaokul</option><option value="3">Yüksek Okul</option><option value="4">Meslek Yüksek Okulu</option><option value="5">Lisans</option><option value="6">Master</option><option value="7">Doktora</option><option value="8">Hiçbiri</option></select></div><div class="col-sm-4"><label class="control-label">Cinsiyet</label><select class="form-control"><option value="1">Kadın</option><option value="2">Erkek</option></select></div>';
}
/*and add Customers Type*/
setTimeout(function () {
 $(function () {
   $('table').DataTable( {
        "language": {
            "lengthMenu": "_MENU_",
            "zeroRecords": "Veriler Bulunamadı",
            "search":"Birşeyler Ara",
            "info": "_TOTAL_ kayıttan _START_ to _END_ gösteriliyor",
            "infoEmpty": "Kayıt Bulunamadı",
            "paginate": {
                "first":      "Başa Dön",
                "last":       "Son Sayfa",
                "next":       "İleri",
                "previous":   "Geri"
            },
            "infoFiltered": ""
        }
    } );
 });
}, 6000);
$(document).ready(function(){
  $("#customerType").change(function(){
      var value = $(this).val();
      if (value == 0) {

      }
      else if (value == 1) {
        $(".Corporate").hide();
        $(".Personal").show();
      }
      else if (value == 2) {
        $(".Personal").hide();
        $(".Corporate").show();
      }
  });
  $("#addCustomerType").change(function(){
      var $value = $(this).find('option:selected').data("value");
      switch ($value) {
        case  'kisisel':
            $(".kurumsal").hide();
            $(".yabanci").hide();
            $(".kisisel").show();
          break;
        case 'kurumsal':
            $(".kurumsal").hide();
            $(".yabanci").hide();
            $(".kisisel").show();
          break;
        case 'yabanci':
            
          break;
        default:

      }
  });
  $(".date").dateDropper();
});
