import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AuthGuard, AuthenticationService } from './servis/index';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MenuComponent } from './navbar/menu';
import {
CustomersComponent,
sozlesmeComponent,
bayiComponent,
subeComponent,
addCustomersComponent,
addDelearComponent,
DeviceComponent
} from './pages/index';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    NavbarComponent,
    MenuComponent,
    CustomersComponent,
    DeviceComponent,
    bayiComponent,
    subeComponent,
    sozlesmeComponent,
    addCustomersComponent,
    addDelearComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {
        path:"login",
        component:LoginComponent
      },
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      {path:'home', component:HomeComponent, canActivate:[AuthGuard]},
      {path:'list-customers', component:CustomersComponent, canActivate:[AuthGuard]},
      {path:'list-agreements', component:sozlesmeComponent, canActivate:[AuthGuard]},
      {path:'list-dealers', component:bayiComponent, canActivate:[AuthGuard]},
      {path:'list-branch', component:subeComponent, canActivate:[AuthGuard]},
      {path:'create-dealers', component:addDelearComponent, canActivate:[AuthGuard]},
      {path:'create-customers', component:addCustomersComponent, canActivate:[AuthGuard]},
      {path:'list-device', component:DeviceComponent, canActivate:[AuthGuard]},
    ])
  ],
  providers: [
    AuthGuard,
    AuthenticationService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}
