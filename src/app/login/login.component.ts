import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../servis/authServis';
import {AuthGuard} from '../servis/guard';
import {AlertService} from '../servis/alerts';
import {ResponseServices} from '../servis/response';
import { Router, CanActivate } from '@angular/router';
import { SweetAlertService } from 'ng2-sweetalert2';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [AuthenticationService, AuthGuard,AlertService, ResponseServices,SweetAlertService]
})
export class LoginComponent implements OnInit {
  model: any = {};
  paste:string;
  constructor(
    private _authServis:AuthenticationService,
    private route:Router,
    private alert:AlertService,
    private sweet:SweetAlertService,
    private responseConvert:ResponseServices) { }

  ngOnInit() {
  }

  login(){
    console.log(this.model.username);
    return this._authServis.giris(this.model.username,this.model.password).
    subscribe(
      data=>{
        location.href='/';
      },
      error=>{
        let res = error.json();
        let jsonError = JSON.parse(res.error);
        // let json = jsonError.replace('\"','"');
        this.alert.swalFunction("Hata", this.responseConvert.responseService(jsonError.ResponseCode),"error");
      }
    )
    ;
  }

}
