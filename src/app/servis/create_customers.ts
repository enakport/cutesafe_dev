import { Injectable } from '@angular/core';
import{ Router} from '@angular/router';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class createCustomers {
    
    constructor(private _http: Http, router:Router) { }
    varUrlPersonal:string = 'http://52.233.193.197:15203/api/admin/customer/createpersonalcustomer';
    varUrlForeigner:string = 'http://52.233.193.197:15203/api/admin/customer/createpersonalcustomer';
    varUrlCorporate:string = 'http://52.233.193.197:15203/api/admin/customer/createpersonalcustomer';
    makeid() {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

      return text;
    }

    Customer_CreateForeignerCustomer(){

    }
    Customer_CreateCorporateCustomer(){

    }

    Customer_CreatePersonalCustomer(
        name:string,
        surname:string,
        tckn:string, 
        customerClass:string,
        gender:string,
        birthPlace:string
        ){
        let personelJson = {
      PersonalCustomerRequest: {
        CustomerBaseRequest: {
          MembershipStartDate: '2017-11-15T11:39:36.981Z',
          CustomerClass: customerClass,
          CancellationReason: 'ReasonNotDefined',
          PassiveReason: 'PaymentNotMade',
          LegalProceedingState: 'Warning'
        },
        PersonalCustomerBaseRequest: {
          Tckn: tckn,
          PersonalCustomerUpdateRequest: {
            Name: name,
            Surname: surname,
            EducationStatus: 'PrimarySchool',
            Gender: gender,
            BirthPlace: birthPlace,
            BirthDate: '2017-11-15T11:39:36.983Z',
            NationalityId: 1,
            IdentitySerialNumber: '1',
            IdentityNumber: '1',
            VolumeNumber: '1',
            FamilyNumber: 'a1',
            ItemNumber: '1'
          }
        }
      },
      CurrentAccountRequest: {
        CurrentAccountCode: this.makeid(),
        CurrentAccountName: this.makeid(),
        LastAgreementDate: '2017-11-15T11:39:36.983Z',
        CurrentAccountType: 'CustomerCurrentAccount'
      }
    };
    	let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('type_of')+" "+localStorage.getItem('_token'));
        headers.append('Accept', 'Application/json');
        headers.append('Content-Type', 'Application/json');
        let options = new RequestOptions({ headers:headers });
        return this._http.post(this.varUrlPersonal,JSON.stringify(personelJson),options).map((response:Response)=>response.json());
    }
}
