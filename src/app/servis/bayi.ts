import { Injectable } from '@angular/core';
import{ Router} from '@angular/router';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
@Injectable()
export class bayiList {
    baseUrl: string = 'http://52.233.193.197:15203/api/admin/dealer/getall/skip/0/take/10';
    constructor(private http:Http) {

    }
      list(){
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('type_of')+" "+localStorage.getItem('_token'));
        let options = new RequestOptions({ headers:headers });
        return this.http.get(this.baseUrl, options).map((response:Response) =>response.json());
      }
      
}
