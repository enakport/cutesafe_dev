import { Injectable } from '@angular/core';
import{ Router} from '@angular/router';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
@Injectable()
export class customersList {
    baseUrlPersonal: string = 'http://52.233.193.197:15203/api/admin/customer/getall/customertype/Personal/skip/0/take/20';
    baseUrlCorporate: string = 'http://52.233.193.197:15203/api/admin/customer/getall/customertype/Corporate/skip/0/take/20';
    baseUrlForeigner: string = 'http://52.233.193.197:15203/api/admin/customer/getall/customertype/Foreigner/skip/0/take/20';
    constructor(private http:Http) {

    }
      Customer_CreatePersonalCustomer(){
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('type_of')+" "+localStorage.getItem('_token'));
        let options = new RequestOptions({ headers:headers });
        return this.http.get(this.baseUrlPersonal, options).map((response:Response) =>response.json());
      }
      Customer_CreateCorporateCustomer(){
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('type_of')+" "+localStorage.getItem('_token'));
        let options = new RequestOptions({ headers:headers });
        return this.http.get(this.baseUrlCorporate, options).map((response:Response) =>response.json());
      }
      Customer_CreateForeignerCustomer(){
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('type_of')+" "+localStorage.getItem('_token'));
        let options = new RequestOptions({ headers:headers });
        return this.http.get(this.baseUrlForeigner, options).map((response:Response) =>response.json());
      }
      swalFunction(title:string, text:string, type:string){
        // return swal({
        //     "title":title,
        //     "text":text,
        //     "type":type,
        //     confirmButtonText: 'Tammam!'
        // });
      }
}
