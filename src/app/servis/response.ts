import { Injectable } from '@angular/core';

@Injectable()
export class ResponseServices {
    constructor() { }

      responseService(statusCode:string ){
        if(statusCode == "1100"){
          return 'Böyle Bir Kayıt Yok';
        }
        if(statusCode == "1202"){
          return 'Şifre Hatalı';
        }
      }
}
