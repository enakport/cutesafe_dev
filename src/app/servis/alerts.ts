import { Injectable } from '@angular/core';
import { SweetAlertService } from 'ng2-sweetalert2';
declare let swal: any;
@Injectable()
export class AlertService {
    constructor() { }

      swalFunction(title:string, text:string, type:string){
        return swal({
            "title":title,
            "text":text,
            "type":type,
            confirmButtonText: 'Tammam!'
        });
      }
}
