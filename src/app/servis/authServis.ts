import { Injectable } from '@angular/core';
import{ Router} from '@angular/router';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class AuthenticationService {
    constructor(private _http: Http, router:Router) { }

      giris(username:string, password:string){
        let headers = new Headers();
        let body = "grant_type=password&username="+username+"&password="+password+"&clientpassword=4645&clientcode=web";
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        let options = new RequestOptions({ headers: headers });
        return this._http.post("http://52.233.193.197:15203/token", body,options).map((response: Response) => {
        let resp = response.json();
        if (resp.access_token) {
            localStorage.setItem('_token', resp.access_token);
            localStorage.setItem('expires_in', resp.expires_in);
            localStorage.setItem('type_of', resp.token_type);
            // this.router.navigate(['/home']);
          }
          else {
            let error = JSON.stringify(resp.error);
            console.log(error);
          }
        });
      }
}
