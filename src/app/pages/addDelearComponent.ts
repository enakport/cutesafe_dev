import { Component, OnInit } from '@angular/core';
import {AuthGuard} from '../servis/guard';
import {AlertService} from '../servis/alerts';
import {createCustomers} from '../servis/index';
import {ResponseServices} from '../servis/response';
import { Router, CanActivate,ActivatedRoute } from '@angular/router';
import { SweetAlertService } from 'ng2-sweetalert2';
import { Form } from '@angular/forms';

@Component({
  selector: 'app-delears',
  templateUrl: '../html_pages/bayi_ekle.html',
  providers:[createCustomers,AlertService, ResponseServices,SweetAlertService],
})
export class addDelearComponent implements OnInit {

  constructor(
      private _add:createCustomers,
      private alert:AlertService,
      private sweet:SweetAlertService,
      private _route:ActivatedRoute,
      private responseConvert:ResponseServices
    ){

  }
  model:any={};
  loading = false;
  page:string;
  type:string;
  personal = false;
  company = false;
  foreign = false;
  ngOnInit(): void{
 
  }
  getType(value){
    if (value == 1) {
      this.personal = true;
    }
    else if (value == 2) {
      this.personal = false;
    }
  }
  addCustomers(){
  
 	if (this.model.customerType == 1) {
     this.loading = true;
 	 	return this._add.Customer_CreatePersonalCustomer(

      this.model.Name, 
      this.model.Surname, 
      this.model.Tckn,
      this.model.customerClass,
      this.model.gender,
      this.model.BirthPlace
      ).subscribe(
 	 		data=>{
         let dataJson = data.json();
        this.loading = false;
        this.alert.swalFunction("Hata", dataJson.Message,"success");
        setTimeout(()=>{    
             window.location.href='/list-customers';
         },3000);
 	 			console.log(data.json());
 	 		},
 	 		error=>{
        this.loading = false;
        let res = error.json();
        let jsonError = res;
        // let json = jsonError.replace('\"','"');
        this.alert.swalFunction("Hata", jsonError.Message,"error");
 	 		}
 	 	);
 	}
  }
}
