import { Component, OnInit } from '@angular/core';
import {subeList} from '../servis/index';
import {AuthGuard} from '../servis/guard';
import {AlertService} from '../servis/alerts';
import {ResponseServices} from '../servis/response';
import { Router, CanActivate } from '@angular/router';
import { SweetAlertService } from 'ng2-sweetalert2';
import * as $ from 'jquery';
@Component({
  selector: 'app-sube',
  templateUrl: '../html_pages/sube.html',
  providers:[subeList],
})
export class subeComponent implements OnInit {

  constructor(private list:subeList) { }
  data = [];
  contact = [];
  json:string;
  ngOnInit() {
  	this.list.list().subscribe(
  		data=>{
  			let json = data;

  			this.data = json.Result.EntityList;
        for(var i = 0;i<json.Result.EntityList.length;i++){
          this.list.idList(json.Result.EntityList[i].Id).subscribe(
              data=>{
                  let array = {
                        PhoneNumber:""+data.Result.EntityList[0].PhoneNumber+""                    
                  };
                  // this.data.push(array);
                  let jsonData = json.Result.EntityList[i];
                  this.contact.push(data.Result.EntityList[0]);

              },
              error=>{

              }
          );
        }
        console.log(this.contact);
  		},
  		error=>{

  		}

  	);
   this.json = JSON.stringify(this.contact);
  }


}
