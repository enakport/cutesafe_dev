import { Component, OnInit } from '@angular/core';
import {customersList} from '../servis/index';
import {AuthGuard} from '../servis/guard';
import {AlertService} from '../servis/alerts';
import {ResponseServices} from '../servis/response';
import { Router, CanActivate } from '@angular/router';
import { SweetAlertService } from 'ng2-sweetalert2';

@Component({
  selector: 'app-customers',
  templateUrl: '../html_pages/customers.html',
  providers:[customersList],
})
export class CustomersComponent implements OnInit {
  settings = {
  edit:{
    editButtonContent:"Düzenle",
  },
  noDataMessage:"Müşteri Listesi Bulunamadı",
  columns: {
    Name: {
      title: 'Adı',
      editable: false,
      addable: false,
      filter:false
    },
    Surname: {
      title: 'Soyadı',
      editable: false,
      addable: false,
      filter:false
    },
    CustomerType: {
      title: 'Müşteri Tipi',
      editable: false,
      addable: false,
      filter:false
    },
  }
};
  data = [];
  kisisel = [];
  kurumsal = [];
  yabanci = [];
  
  constructor(private list:customersList) { }

  ngOnInit() {
    
    this.list.Customer_CreatePersonalCustomer().subscribe(
        data=>{
          this.kisisel = data.Result.EntityList;
          this.data = data.Result.EntityList;

        },
        error=>{

        }
    );
    this.list.Customer_CreateCorporateCustomer().subscribe(
        data=>{
          this.kurumsal = data.Result.EntityList;
          this.data = data.Result.EntityList;

        },
        error=>{

        }
    );
    this.list.Customer_CreateForeignerCustomer().subscribe(
        data=>{
          this.yabanci = data.Result.EntityList;
          this.data = data.Result.EntityList;

        },
        error=>{

        }
    );
  }

  Customer_CreateForeignerCustomer(){

  }
  Customer_CreateCorporateCustomer(){

  }

  Customer_CreatePersonalCustomer(){

  }

}
