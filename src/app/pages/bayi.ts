import { Component, OnInit } from '@angular/core';
import {bayiList} from '../servis/index';
import {AuthGuard} from '../servis/guard';
import {AlertService} from '../servis/alerts';
import {ResponseServices} from '../servis/response';
import { Router, CanActivate } from '@angular/router';
import { SweetAlertService } from 'ng2-sweetalert2';
import * as $ from 'jquery';
@Component({
  selector: 'app-bayi',
  templateUrl: '../html_pages/bayi.html',
  providers:[bayiList],
})
export class bayiComponent implements OnInit {

  constructor(private list:bayiList) { }
  data = [];
  ngOnInit() {
  	this.list.list().subscribe(
  		data=>{
  			let json = data;

  			this.data = json.Result.EntityList;

  		},
  		error=>{

  		}
  	);
  }


}
