import { Component, OnInit } from '@angular/core';
import {sozlesmeList} from '../servis/index';
import {AuthGuard} from '../servis/guard';
import {AlertService} from '../servis/alerts';
import {ResponseServices} from '../servis/response';
import { Router, CanActivate } from '@angular/router';
import { SweetAlertService } from 'ng2-sweetalert2';

@Component({
  selector: 'app-sozlesme',
  templateUrl: '../html_pages/sozlesme.html',
  providers:[sozlesmeList]
})
export class sozlesmeComponent implements OnInit {

  rows = [];
  constructor(private list:sozlesmeList) { }

  ngOnInit() {
    this.list.Customer_CreatePersonalCustomer().subscribe(
        data=>{
          this.rows = data.Result.EntityList;
          console.log(this.rows);
        },
        error=>{

        }
    );
  }

}
